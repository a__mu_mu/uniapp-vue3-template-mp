export const devService = {
    demoService: 'https://xx.com', //测试环境

}

export const testService = {
    demoService: 'https://xx.com', //测试环境

}

export const preService = {
    demoService: 'https://xx.com', //生产环境

}

export const proService = {
    demoService: 'https://xx.com', //生产环境

}

export const getService = (name = 'demoService') => {
    switch (process.env.VITE_APP_ENV) {
        case 'dev':
            {
                return devService[name]
            }

        case 'test':
            {
                return testService[name]
            }

        case 'pre':
            {
                return preService[name]
            }

        case 'prod':
            {
                return proService[name]
            }
    }
}

// 输出日志信息
export const noConsole = false
