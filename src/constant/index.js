/*
 * @Author: donggq donggq@toplion.com.cn
 * @Date: 2023-07-17 17:22:51
 * @LastEditors: donggq donggq@toplion.com.cn
 * @LastEditTime: 2023-07-24 14:48:59
 * @FilePath: \convenient-parking-mp\src\constant\index.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */

// import {enum2Array} from '@/utils/index'



export const SERVICE_NAME = {
    parking: 'parkingService',
}

export const TAB_BAR = {
    OPERATOR: 'operator',
    PARTNER: 'partner', 
    SHOPOWNER: 'shopowner',
    USER: 'user',
}
