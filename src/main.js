
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import { createSSRApp } from 'vue'
import App from './App.vue'
import componentsInstall from './components'
import CustomTabbar from '@/components/custom-tabbar'

export function createApp() {
    const app = createSSRApp(App)
    getSystemInfo(app)
    const pinia = createPinia()
    pinia.use(piniaPluginPersistedstate)
    app.use(pinia)
	app.component('CustomTabbar',CustomTabbar)
	
    return {
        app,
    }
}

function getSystemInfo(app) {
    uni.getSystemInfo({
        success: e => {
            console.log('getsysteminfo', e)

            // #ifndef MP
            app.config.globalProperties.$StatusBar = e.statusBarHeight
            if (e.platform == 'android') {
                app.config.globalProperties.$CustomBar = e.statusBarHeight + 50
            } else {
                app.config.globalProperties.$CustomBar = e.statusBarHeight + 45
            }
            // #endif

            // #ifdef MP-WEIXIN
            app.config.globalProperties.$StatusBar = e.statusBarHeight
            const custom = wx.getMenuButtonBoundingClientRect()
            console.log(custom)
            app.config.globalProperties.$Custom = custom
            app.config.globalProperties.$CustomBar = custom.height + (custom.top - e.statusBarHeight) * 2
            // #endif
        },
    })
}
