
import CustomTabbar from './custom-tabbar'


const components = [
    CustomTabbar
]
export default {
    install(app){
        console.log('install',app)
        components.forEach(component => {
            app.component('CustomTabbar', component)
        })
    }
}
