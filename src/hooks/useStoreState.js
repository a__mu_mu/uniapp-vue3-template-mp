import { useMainStore,useUserStore } from '@/store'
import { storeToRefs } from 'pinia'


export const useStoreState = (module = 'main') => {

    const getModuleStore = (module) => {
        console.log(module,useMainStore)
        const storeModule = {
            'main':useMainStore,
            'user':useUserStore
        }

        const useStore = storeModule[module]
        const store = useStore()
        return store
    }
    const store = getModuleStore(module)

    return {
        store: store,
        ...storeToRefs(store)
    }
}

export default useStoreState
