import { defineConfig } from 'vite'
import uni from '@dcloudio/vite-plugin-uni'
import { resolve } from 'path'
import AutoImport from 'unplugin-auto-import/vite'
// https://vitejs.dev/config/
export default defineConfig({
    transpileDependencies: ['@climblee/uv-ui'],
    resolve: {
        alias: [
            {
                find: '@',
                replacement: resolve(__dirname, 'src'),
            },
        ],
    },
    plugins: [
        uni(),
        AutoImport({
            imports: [
                // 预设
                'vue',
                'uni-app',
            ],
        }),
    ],
    define: {
        'process.env.VITE_APP_ENV': JSON.stringify(process.env.VITE_APP_ENV),
    },
})
